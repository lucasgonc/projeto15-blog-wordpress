# ASG
resource "aws_appautoscaling_target" "ecs_service_as" {
  max_capacity       = var.max-tasks
  min_capacity       = var.min-tasks
  resource_id        = "service/${aws_ecs_cluster.projeto-cluster.name}/${aws_ecs_service.projeto.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "scale-to-memory" {
  name               = "scale-to-memory"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_service_as.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_service_as.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_service_as.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageMemoryUtilization"
    }

    target_value = 80
  }
}

resource "aws_appautoscaling_policy" "scale-to-cpu" {
  name               = "scale-to-cpu"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_service_as.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_service_as.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_service_as.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }

    target_value = 70
  }
}
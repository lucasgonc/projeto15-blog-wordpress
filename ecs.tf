resource "aws_ecs_cluster" "projeto-cluster" {
  name = "projeto-cluster-${var.ecr_name}"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_ecs_cluster_capacity_providers" "ecs_provider" {
  cluster_name = aws_ecs_cluster.projeto-cluster.name

  capacity_providers = ["FARGATE"]

  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = "FARGATE"
  }
}

resource "aws_ecs_task_definition" "projeto" {
  family                   = var.ecr_name
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = var.ecs-vcpu
  memory                   = var.ecs-memory
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  task_role_arn            = aws_iam_role.ecs_task_execution_role.arn

  volume {
    name = var.ecr_name

    efs_volume_configuration {
      file_system_id          = aws_efs_file_system.projeto-efs.id
      transit_encryption      = "ENABLED"
      transit_encryption_port = 2049
      authorization_config {
        access_point_id = aws_efs_access_point.efs-access-point.id

      }
    }
  }

  container_definitions = <<EOF
  [
      {
          "portMappings": [
              {
                  "containerPort": 8080,
                  "protocol": "tcp"        
              }
          ],
          "essential": true,
          "mountPoints": [
              {
                  "sourceVolume": "wordpress",
                  "containerPath": "/bitnami/wordpress",
                  "readOnly": false
              }
          ],
          "name": "${var.ecr_name}",
          "image": "${local.ecr_reg}:latest",
          "logConfiguration": {
              "logDriver": "awslogs",
              "options": {
                  "awslogs-group": "/ecs/${var.ecr_name}",
                  "awslogs-region": "${var.regiao}",
                  "awslogs-stream-prefix": "ecs"
              }
          },
          "environment": [
              {
                  "name": "WORDPRESS_DATABASE_HOST",
                  "value": "${aws_db_instance.projeto-rds.address}"
              },
              {   
                  "name": "WORDPRESS_DATABASE_PORT_NUMBER",
                  "value": "3306"
              },
              {   
                  "name": "WORDPRESS_DATABASE_USER",
                  "value": "${var.rds-nome-usuario}"
              },
              {   
                  "name": "WORDPRESS_DATABASE_PASSWORD",
                  "value": "${var.rds-senha-usuario}"
              },
              {   
                  "name": "WORDPRESS_DATABASE_NAME",
                  "value": "${var.rds-nome-banco}"
              },
              {   
                  "name": "WORDPRESS_USERNAME",
                  "value": "user"
              },
              {   
                  "name": "WORDPRESS_PASSWORD",
                  "value": "bitnami"
              },
              {
                "name": "WORDPRESS_SMTP_HOST",
                "value": "email-smtp.${var.regiao}.amazonaws.com"
              },
              {
                "name": "WORDPRESS_SMTP_USER",
                "value": "${aws_iam_access_key.smtp_user.id}"
              },
              {
                "name": "WORDPRESS_SMTP_PASSWORD",
                "value": "${aws_iam_access_key.smtp_user.ses_smtp_password_v4}"
              },
              {
                "name": "WORDPRESS_SMTP_PORT",
                "value": "587"
              },
              {   
                "name": "PHP_MEMORY_LIMIT",
                "value": "512M"
              },
              {   
                "name": "WP_AUTO_UPDATE_CORE",
                "value": "true"
              },
              {   
                "name": "WORDPRESS_ENABLE_HTTPS",
                "value": "yes"
              }
          ]
      }
  ] 
  EOF
}

resource "aws_ecs_service" "projeto" {
  name                               = var.ecr_name
  cluster                            = aws_ecs_cluster.projeto-cluster.id
  task_definition                    = aws_ecs_task_definition.projeto.arn
  desired_count                      = 2
  launch_type                        = "FARGATE"
  platform_version                   = "LATEST"
  deployment_maximum_percent         = 100
  deployment_minimum_healthy_percent = 50
  network_configuration {
    subnets = setunion(
      module.network.private_subnets[*].id
    )
    security_groups  = [module.security.sg-web.id]
    assign_public_ip = false
  }
  deployment_controller {
    type = "ECS"
  }
  lifecycle {
    // Define a "create_before_destroy" behavior to update the service
    create_before_destroy = true
  }
  load_balancer {
    container_name   = var.ecr_name
    container_port   = "8080"
    target_group_arn = aws_lb_target_group.proj_tg.arn
  }
}

resource "aws_cloudwatch_log_group" "projeto" {
  name              = "/ecs/${var.ecr_name}"
  retention_in_days = 7
}

resource "aws_cloudwatch_log_stream" "projeto" {
  name           = "ecs"
  log_group_name = aws_cloudwatch_log_group.projeto.name
}

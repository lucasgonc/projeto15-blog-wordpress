# Renomeie este arquivo, ou crie outro, com nome terraform.tfvars
# Depois adicione os valores desejados para as variáveis abaixo.
# Isto evitará que sejam solicitados valores para estas variávies 
# quando forem executados os comandos do terraform.

# Define a região na AWS que será criada toda infraestrutura
regiao = "us-east-1" # Virginia

# Define o nome do usuário que esta executando este Projeto
usuario = "paulonaka"

# Define as informações do banco de dados RDS
rds-nome-usuario   = "bn_wordpress"
rds-senha-usuario  = "bitnamiword"
rds-identificador  = "mariadb"
rds-nome-banco     = "bitnami_wordpress"
rds-tipo-instancia = "db.t3.micro"

# Nome do profile criado com AWS CLI com as
# credenciais do IAM.
# profile = "default"

# Especifique o nome da tag padrão utilizada nos nomes dos serviços
tag-base = "projeto15"

# Especifique o nome do bucket
nome-bucket = "projeto15-wordpress" # Como o bucket deve ser unico em toda a AWS, sugiro modifica este nome para evitar conflito.

# Email para usar na configuração de envio
# A AWS enviará uma solicitação de confirmação para validar
user-email = "admin@nakamatsu.cloud"

# Configurações mínima e máxima para o autoscaling
min-tasks = 2
max-tasks = 4

# Define se serão criados NAT Gateways nas 
# subnets 
use-nat-gateway = true

# Define quantidade de CPU para cada task
ecs-vcpu = (1 * 1024) # 1 vCPU

# Define quantidade de memória para cada task
ecs-memory = (3 * 1024) # 2 GB de RAM

# Define o dominio - Colocar, se existir, seu dominio cadatrado no route53
dominio_certificado = "nakamatsu.cloud"

# Define a tag de Ambiente do repositório ECR
tags = {
  "Environment" = "Projeto15"
}

# Define o nome do repositório - mesmo nome da imagem
ecr_name = "wordpress"

image_mutability = "IMMUTABLE"

# Define a imagem onde deseja fazer uso do Docker Hub
doker_hub_image = "bitnami/wordpress"

# CloudFront
url = "blogupper.nakamatsu.cloud"

url_prefix = "blogupper"

cliente = "blogupper"

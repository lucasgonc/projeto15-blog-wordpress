#Seleciona dominio existente no Route53 - Se não houver dominio, renomear para route53.tf.no
data "aws_route53_zone" "selected" {
  name         = var.dominio_certificado
  private_zone = false
}

# Criação da entrada de DNS para o Origin da distribuição do CLoudFront direcinando para o ALB
resource "aws_route53_record" "origin" {

  zone_id = data.aws_route53_zone.selected.id
  name    = "origin-${var.url_prefix}.${var.dominio_certificado}"
  type    = "A"
  alias {
    name                   = aws_lb.proj_elb.dns_name
    zone_id                = aws_lb.proj_elb.zone_id
    evaluate_target_health = true
  }
}
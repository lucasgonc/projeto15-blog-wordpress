# Download da imagem do repositório Docker Hub e Upload o repositório ECR
locals {
  ecr_reg = module.ecr-repo.image_uri.repository_url # ECR docker registry URI

  dkr_build_cmd = <<-EOT
        docker pull ${var.doker_hub_image}
        docker tag ${var.doker_hub_image}:latest ${local.ecr_reg}:latest
        aws ecr get-login-password --region ${var.regiao} | docker login --username AWS --password-stdin ${data.aws_caller_identity.current.account_id}.dkr.ecr.${var.regiao}.amazonaws.com
        docker push ${local.ecr_reg}:latest
    EOT
}

# local-exec for build and push of docker image
resource "terraform_data" "build_push_dkr_img" {
  depends_on = [module.ecr-repo]

  provisioner "local-exec" {
    command    = local.dkr_build_cmd
    on_failure = continue
  }
}



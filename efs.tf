# Criando EFS
resource "aws_efs_file_system" "projeto-efs" {
  creation_token = "efsvolume" # Usado posteriormente com AWS CLI para montar o EFS
  tags = {
    Name = "${var.ecr_name}-EFS"
  }
}

resource "aws_efs_mount_target" "projeto-efs-mt" {
  count           = length(module.network.public_subnets)
  file_system_id  = aws_efs_file_system.projeto-efs.id
  subnet_id       = module.network.public_subnets[count.index].id
  security_groups = [module.security.sg-efs.id]
}

resource "aws_efs_access_point" "efs-access-point" {
  file_system_id = aws_efs_file_system.projeto-efs.id
  posix_user {
    uid = "1001"
    gid = "1001"
  }
  root_directory {
    creation_info {
      owner_gid   = "1001"
      owner_uid   = "1001"
      permissions = "0777"
    }
    path = "/bitnami"
  }
}
# Módulo de criação dos Security Groups para utilização no projeto
module "security" {
  source     = "./module-web-security"
  db-name    = "MariaDB"
  db-port    = 3306
  region     = var.regiao
  tags-sufix = var.tag-base
  vpc-id     = module.network.vpc_id
}

# Security outputs
# module.security.sg-web
# module.security.sg-elb
# module.security.sg-db
# module.security.sg-efs
# module.security.sg-cache
# module.security.sg-ses
# module.security.sg-all


# Arquivo com a definição das variáveis. O arquivo poderia ter qualquer outro nome, ex. valores.tf

# Definição da Região na AWS
variable "regiao" {
  description = "Região da AWS para provisionamento"
  type        = string
  # Veja o arquivo terraform.tfvars.exemplo para definir um valor fixo para esta variável.
}

# Definição do profile de conta que será utilizado com as credenciais de acesso.
# variable "profile" {
#   description = "Profile com as credenciais criadas no IAM"
#   type        = string
# Veja o arquivo terraform.tfvars.exemplo para definir um valor fixo para esta variável.
# }

# Definição do usuário que esta criando a infra na AWS.
variable "usuario" {
  description = "Nome do usuário que esta criando a infra na AWS"
  type        = string
  # Veja o arquivo terraform.tfvars.exemplo para definir um valor fixo para esta variável.
}

# Definição de tag para o Projeto - Todos os recursos serão tageados com esta informação.
variable "tag-base" {
  description = "Nome utilizado para nomenclaruras no projeto"
  type        = string
  # Veja o arquivo terraform.tfvars.exemplo para definir um valor fixo para esta variável.
}

# RDS
variable "rds-identificador" {
  description = "Tipo da instância do RDS"
  type        = string
  # default     = "nao colocar valor padrão aqui" # Não deixar padrão para versionar com git.
  # Veja o arquivo terraform.tfvars.exemplo para definir um valor fixo para esta variável.
}
variable "rds-tipo-instancia" {
  description = "Tipo da instância do RDS"
  type        = string
  # default     = "nao colocar valor padrão aqui" # Não deixar padrão para versionar com git.
  # Veja o arquivo terraform.tfvars.exemplo para definir um valor fixo para esta variável.
}
variable "rds-nome-banco" {
  description = "Nome do schema criado inicialmente para usar no Projeto"
  type        = string
  # default     = "nao colocar valor padrão aqui" # Não deixar padrão para versionar com git.
  # Veja o arquivo terraform.tfvars.exemplo para definir um valor fixo para esta variável.
}
variable "rds-nome-usuario" {
  description = "Nome do usuário administrador da instância RDS"
  type        = string
  # default     = "nao colocar valor padrão aqui" # Não deixar padrão para versionar com git.
  # Veja o arquivo terraform.tfvars.exemplo para definir um valor fixo para esta variável.
}
variable "rds-senha-usuario" {
  description = "Senha do usuário administrador da instância RDS"
  type        = string
  # default     = "nao colocar valor padrão aqui" # Não deixar padrão para versionar com git.
  # Veja o arquivo terraform.tfvars.exemplo para definir um valor fixo para esta variável.
}

# S3
variable "nome-bucket" {
  description = "Nome do bucket para configurar no Projeto"
  type        = string
  # Veja o arquivo terraform.tfvars.exemplo para definir um valor fixo para esta variável.
}

# SES
variable "user-email" {
  description = "Email do Admin para teste do SES"
  type        = string
  # Veja o arquivo terraform.tfvars.exemplo para definir um valor fixo para esta variável.
}

# ASG - Configurações mínima e máxima para o autoscaling
variable "min-tasks" {
  description = "Quantidade mínima de tasks no autoscaling"
  type        = number
  # Veja o arquivo terraform.tfvars.exemplo para definir um valor fixo para esta variável.
}
variable "max-tasks" {
  description = "Quantidade máxima de tasks no autoscaling"
  type        = number
  # Veja o arquivo terraform.tfvars.exemplo para definir um valor fixo para esta variável.
}

# ECS - Task Definition
variable "ecs-vcpu" {
  description = "Quantidade máxima de tasks no autoscaling"
  type        = number
  # Veja o arquivo terraform.tfvars.exemplo para definir um valor fixo para esta variável.
}
variable "ecs-memory" {
  description = "Quantidade máxima de tasks no autoscaling"
  type        = number
  # Veja o arquivo terraform.tfvars.exemplo para definir um valor fixo para esta variável.
}

# Define se serão criados NAT Gateways nas subnets 
variable "use-nat-gateway" {
  description = "Especifica se serão criados NAT Gateways para cada Subnet pública."
  type        = bool
  # Veja o arquivo terraform.tfvars.exemplo para definir um valor fixo para esta variável.
}

# Route53 - Dominio a ser utilizado
variable "dominio_certificado" {
  description = "Especifica qual o dominio existente para criação do certificado publico"
  type        = string
  # Veja o arquivo terraform.tfvars.exemplo para definir um valor fixo para esta variável.
}

# CloudFront
variable "url" {
  description = "URL do site - Necessário um dóminio e a criação do certificado SSL"
  type        = string

}
variable "url_prefix" {
  description = "URL do site - Necessário um dóminio e a criação do certificado SSL"
  type        = string

}
variable "cliente" {
  description = "Nome do Cliente, base para nomear os recursos"
  type        = string

}

# Criação do repositório de imagem docke ECR
variable "ecr_name" {
  description = "The list of ecr names to create"
  type        = string
  # Veja o arquivo terraform.tfvars.exemplo para definir um valor fixo para esta variável.
}
variable "tags" {
  description = "The key-value maps for tagging"
  type        = map(string)
  default     = {}
}
variable "image_mutability" {
  description = "Provide image mutability"
  type        = string
  default     = "MUTABLE"
}
variable "encrypt_type" {
  description = "Provide type of encryption here"
  type        = string
  default     = "KMS"
}

# Imagem que será utilizado do Docker Hub
variable "doker_hub_image" {
  description = "Imagem no Docker Hub"
  type        = string
  # Veja o arquivo terraform.tfvars.exemplo para definir um valor fixo para esta variável.
}
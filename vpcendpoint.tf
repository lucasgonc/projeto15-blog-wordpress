# VPC Endpoint ECR
resource "aws_vpc_endpoint" "ecr_endpoint_api" {
  count             = var.use-nat-gateway ? 0 : 1
  vpc_id            = module.network.vpc_id
  service_name      = "com.amazonaws.${var.regiao}.ecr.api"
  vpc_endpoint_type = "Interface"

  private_dns_enabled = true
  security_group_ids  = [module.security.sg-ecr.id]
  subnet_ids          = module.network.private_subnets[*].id # Select the appropriate private subnet

  tags = {
    Name = "ecr-endpoint-api"
  }
}

resource "aws_vpc_endpoint" "ecr_endpoint_dkr" {
  count             = var.use-nat-gateway ? 0 : 1
  vpc_id            = module.network.vpc_id
  service_name      = "com.amazonaws.${var.regiao}.ecr.dkr"
  vpc_endpoint_type = "Interface"

  private_dns_enabled = true
  security_group_ids  = [module.security.sg-ecr.id]
  subnet_ids          = module.network.private_subnets[*].id # Select the appropriate private subnet

  tags = {
    Name = "ecr-endpoint-dkr"
  }
}

# VPC Endpoint Elastic Cache
resource "aws_vpc_endpoint" "elasticache_endpoint" {
  count             = var.use-nat-gateway ? 0 : 1
  vpc_id            = module.network.vpc_id
  service_name      = "com.amazonaws.${var.regiao}.elasticache"
  vpc_endpoint_type = "Interface"

  private_dns_enabled = true
  security_group_ids  = [module.security.sg-ecr.id]
  subnet_ids          = module.network.private_subnets[*].id # Select the appropriate private subnet

  tags = {
    Name = "elasticcache-endpoint"
  }
}

# VPC Endpoint CloudWatch
resource "aws_vpc_endpoint" "cloudwatch_logs" {
  count             = var.use-nat-gateway ? 0 : 1
  vpc_id            = module.network.vpc_id
  service_name      = "com.amazonaws.${var.regiao}.logs"
  vpc_endpoint_type = "Interface"

  private_dns_enabled = true
  security_group_ids  = [module.security.sg-all.id]
  subnet_ids          = module.network.private_subnets[*].id # Select the appropriate private subnet

  tags = {
    Name = "cloudwatch-endpoint-logs"
  }
}

resource "aws_vpc_endpoint" "cloudwatch_monitoring" {
  count             = var.use-nat-gateway ? 0 : 1
  vpc_id            = module.network.vpc_id
  service_name      = "com.amazonaws.${var.regiao}.monitoring"
  vpc_endpoint_type = "Interface"

  private_dns_enabled = true
  security_group_ids  = [module.security.sg-all.id]
  subnet_ids          = module.network.private_subnets[*].id # Select the appropriate private subnet

  tags = {
    Name = "cloudwatch-endpoint-monitoring"
  }
}

# VPC Endpoint EFS
resource "aws_vpc_endpoint" "efs_endpoint_api" {
  count             = var.use-nat-gateway ? 0 : 1
  vpc_id            = module.network.vpc_id
  service_name      = "com.amazonaws.${var.regiao}.elasticfilesystem"
  vpc_endpoint_type = "Interface"

  private_dns_enabled = true
  security_group_ids  = [module.security.sg-all.id]
  subnet_ids          = module.network.private_subnets[*].id # Select the appropriate private subnet

  tags = {
    Name = "efs-endpoint-api"
  }
}

# VPC Endpoint S3
resource "aws_vpc_endpoint" "s3_endpoint" {
  # count             = var.use-nat-gateway ? 0 : 1
  vpc_id            = module.network.vpc_id
  service_name      = "com.amazonaws.${var.regiao}.s3"
  vpc_endpoint_type = "Gateway"

  route_table_ids = module.network.private_route_tables[*].id

  tags = {
    Name = "s3-endpoint"
  }
}

# VPC Endpoint ELB
resource "aws_vpc_endpoint" "elb_endpoint" {
  count             = var.use-nat-gateway ? 0 : 1
  vpc_id            = module.network.vpc_id
  service_name      = "com.amazonaws.${var.regiao}.elasticloadbalancing"
  vpc_endpoint_type = "Interface"

  private_dns_enabled = true
  security_group_ids  = [module.security.sg-all.id]
  subnet_ids          = module.network.private_subnets[*].id # Select the appropriate private subnet

  tags = {
    Name = "elb-endpoint"
  }
}

# VPC Endpoint ECS
resource "aws_vpc_endpoint" "ecs_endpoint" {
  count             = var.use-nat-gateway ? 0 : 1
  vpc_id            = module.network.vpc_id
  service_name      = "com.amazonaws.${var.regiao}.ecs"
  vpc_endpoint_type = "Interface"

  private_dns_enabled = true
  security_group_ids  = [module.security.sg-all.id]
  subnet_ids          = module.network.private_subnets[*].id # Select the appropriate private subnet

  tags = {
    Name = "ecs-endpoint"
  }
}

resource "aws_vpc_endpoint" "ecs_agent_endpoint" {
  count             = var.use-nat-gateway ? 0 : 1
  vpc_id            = module.network.vpc_id
  service_name      = "com.amazonaws.${var.regiao}.ecs-agent"
  vpc_endpoint_type = "Interface"

  private_dns_enabled = true
  security_group_ids  = [module.security.sg-all.id]
  subnet_ids          = module.network.private_subnets[*].id # Select the appropriate private subnet

  tags = {
    Name = "ecs-agent-endpoint"
  }
}

# VPC Endpoint SES
resource "aws_vpc_endpoint" "ses_endpoint" {
  count             = var.use-nat-gateway ? 0 : 1
  vpc_id            = module.network.vpc_id
  service_name      = "com.amazonaws.${var.regiao}.email-smtp"
  vpc_endpoint_type = "Interface"

  private_dns_enabled = true
  security_group_ids  = [module.security.sg-ses.id]
  subnet_ids          = module.network.private_subnets[*].id # Select the appropriate private subnet

  tags = {
    Name = "ses-endpoint"
  }
}

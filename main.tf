# Configurações inicias do projeto com AWS
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.34.0"
    }
  }
  # Definição do backend - local onde será armazenado o arquivo terraform.tfstate do projeto
  backend "s3" {
    bucket = "terraform-tfstate-wordpress-paulonaka"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}

provider "aws" {

  region = var.regiao

  default_tags {
    tags = {
      Criado_por = var.usuario
      IaC        = "terraform"
    }
  }
}

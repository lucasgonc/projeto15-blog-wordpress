# output "memcached-node-endpoint" {
#   value = aws_elasticache_cluster.memcached.cache_nodes.0.address
# }

output "projeto-efs_id" {
  value = aws_efs_file_system.projeto-efs.id
}

output "projeto-lb-dns" {
  // Existing output

  // Add the following output to display the CloudWatch Logs group and stream names
  value = {
    lb_dns     = aws_lb.proj_elb.dns_name
    log_group  = aws_cloudwatch_log_group.projeto.name
    log_stream = aws_cloudwatch_log_stream.projeto.name
    zoneid     = aws_lb.proj_elb.zone_id
  }
}

output "nome-bucket" {
  value = aws_s3_bucket.projeto-static.bucket
}

output "smtp_username" {
  value = aws_iam_access_key.smtp_user.id
}

output "smtp_password" {
  value     = aws_iam_access_key.smtp_user.ses_smtp_password_v4
  sensitive = true
}

output "alb_acm_certificate_arn" {
  value = aws_acm_certificate_validation.elb_cert.certificate_arn
}

data "aws_caller_identity" "current" {}

output "account_id" {
  value = data.aws_caller_identity.current.account_id
}

output "ecr_repo_uri" {
  value = module.ecr-repo.image_uri.repository_url
}

output "ecr_repo_arn" {
  value = module.ecr-repo.image_uri.arn
}

output "ecr_repo_id" {
  value = module.ecr-repo.image_uri.registry_id
}

output "ecr_repo_name" {
  value = module.ecr-repo.image_uri.name
}

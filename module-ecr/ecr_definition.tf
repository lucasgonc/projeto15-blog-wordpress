resource "aws_ecr_repository" "ecr" {
  name                 = var.ecr_name
  image_tag_mutability = var.image_mutability
  force_delete         = true # CUIDADO! Em um ambiente de produção você pode não querer apagar a imagem do repositório
  encryption_configuration {
    encryption_type = var.encrypt_type
  }
  image_scanning_configuration {
    scan_on_push = false
  }
  tags = var.tags
}
# Criando Subnet Group para RDS
resource "aws_db_subnet_group" "projeto-sn-db-group" {
  name = "${var.tag-base}-sn-db-group"
  subnet_ids = setunion(
    module.network.public_subnets[*].id,
    module.network.private_subnets[*].id
  )
}

# Criando Instância de Banco de Dados RDS
resource "aws_db_instance" "projeto-rds" {
  identifier                = var.rds-identificador
  allocated_storage         = 20
  engine                    = "mariadb"
  engine_version            = "10.6.14"
  instance_class            = var.rds-tipo-instancia
  db_name                   = var.rds-nome-banco # Nome do schema criado inicialmente para usar no projeto
  username                  = var.rds-nome-usuario
  password                  = var.rds-senha-usuario
  storage_type              = "gp2"
  multi_az                  = "false" # set to true to have high availability: 2 instances synchronized with each other
  skip_final_snapshot       = true    # Para uso em produção, considerar mudar o valor para false 
  final_snapshot_identifier = "${var.rds-identificador}-bkp"
  # backup_retention_period   = 7
  publicly_accessible    = false
  vpc_security_group_ids = [module.security.sg-db.id]
  db_subnet_group_name   = aws_db_subnet_group.projeto-sn-db-group.id
  tags = {
    Name = "${var.tag-base}-rds"
  }
}

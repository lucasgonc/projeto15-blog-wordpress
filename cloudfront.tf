
module "multi" {
  source = "./module-cloudfront"

  aws_region                = var.regiao
  policy_name               = "policy-cloudfront-${var.url_prefix}"
  lifecycle_environment     = "${var.url}-prod"
  cf_logging_bucket         = format("%s%s%s", "cloudfront-logging-", var.usuario, ".s3.amazonaws.com") #"cloudfront-logging-site.s3.amazonaws.com"
  cf_logging_bucket_s3_name = format("%s%s", "cloudfront-logging-", var.usuario)                        # Usado pelo Cloudformation
  s3_prefix                 = format("%s%s", var.url_prefix, "/")
  alb_acm_certificate_arn   = aws_acm_certificate_validation.elb_cert.certificate_arn
  application_name          = var.url_prefix
  domain_name               = "origin-${var.url}"
  cname                     = var.url
  urlip                     = aws_lb.proj_elb.dns_name
  urlname                   = var.url
  cliente                   = var.cliente
  tags_sufix                = var.tag-base
  dominio_certificado       = var.dominio_certificado
}

# Criação do bucket para armazenamento dos logs do CloudFront para consulta no CloudWatch
module "s3_bucket" {
  source                 = "./s3-bucket"
  cf_logging_bucket_name = format("%s%s", "cloudfront-logging-", var.usuario) #"cloudfront-logging-site"
  prefix                 = var.url_prefix
}